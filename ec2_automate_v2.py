"""
On EC2, admins should be able to tag this instances.
Then this Lambda function will automatically start and stop them on the tagged schedule.

- Sample:
    Tag: RunSchedule
    Value: MTWRF(Start=03:00,Stop=3:40,Start=08:00,Stop=9:00),SaSu(Start=08:00,Stop=9:00)
            M   T    W    R    F    Sa  Su

"""
import boto3
import datetime


def lambda_handler(event, context):
    print "*** Started lambda function: ", datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
    ec2 = boto3.client("ec2")
    reservations = ec2.describe_instances()["Reservations"]
    for each_item in reservations:
        for instance in each_item["Instances"]:
            _id = instance["InstanceId"]
            state = instance["State"]["Name"]
            # Get schedule value
            schedule = None
            for tag in instance['Tags']:
                if tag['Key'] == 'RunSchedule':
                    schedule = tag['Value']
                    break

            if schedule is None:
                print "Invalid type of schedule, ID: ", _id
                break
            try:
                today_sch = get_schedule_of_today(schedule)
                if today_sch is None:
                    break

                action = decide_action(today_sch, state)

                print "ID: ", _id, "  state: ", state, 'action: ', action
                if action == 'start':
                    ec2.start_instances(InstanceIds=[_id, ])
                elif action == 'stop':
                    ec2.stop_instances(InstanceIds=[_id, ])

            except IndexError as e:
                print "Error, ", e


def get_schedule_of_today(schedule):
    """
    Get today's schedule from weekly schedule.
    :param schedule: Weekly schedule.
    :return:
    """
    # Get day of week
    cur_day_index = datetime.date.today().weekday()
    cur_day = ['M', 'T', 'W', 'R', 'F', 'Sa', 'Su'][cur_day_index]

    today_schedule = None
    for buf in schedule.split('),'):
        if cur_day in buf.split('(')[0]:
            today_schedule = buf.split('(')[1]
            break
    if today_schedule is not None:
        if today_schedule.endswith(')'):
            today_schedule = today_schedule[:-1]

    return today_schedule


def decide_action(schedule, state):
    """
    Decide action from the schedule of the day and the current state
    :param schedule: schedule of the day,   i.e. Start=08:00,Stop=16:00,Start=17:00,Stop=19:00
    :param state:   current state of EC2 instance...   'running' or 'stopped'
    :return: action...       'start' or 'stop'
    """
    sch_list = [sch.split('=') for sch in schedule.split(',')]
    cur_elapsed = datetime.datetime.now().hour * 60 + datetime.datetime.now().minute

    action = None
    if cur_elapsed < str_to_min(sch_list[0][1]):  # Before the 1st schedule
        if sch_list[0][0] == 'Start':
            if state == 'running':
                action = 'stop'
        elif sch_list[0][0] == 'Stop':
            if state == 'stopped':
                action = 'start'
    elif cur_elapsed > str_to_min(sch_list[-1][1]):  # After the last schedule
        if sch_list[-1][0] == 'Start':
            if state == 'stopped':
                action = 'start'
        elif sch_list[-1][0] == 'Stop':
            if state == 'running':
                action = 'stop'
    else:
        #  obtain current step in the schedule list
        cur_step = 0
        for i in range(1, len(sch_list)):
            if cur_elapsed < str_to_min(sch_list[i][1]):
                cur_step = i - 1
                break

        if sch_list[cur_step][0] == 'Start':
            if state == 'stopped':
                action = 'start'
        elif sch_list[cur_step][0] == 'Stop':
            if state == 'running':
                action = 'stop'

    return action


def str_to_min(str_val):
    """
    Convert string value to integer in minutes
    04:15  ->  255
    :param str_val:
    :return:
    """
    tmp = str_val.split(':')
    return int(tmp[0]) * 60 + int(tmp[1])
